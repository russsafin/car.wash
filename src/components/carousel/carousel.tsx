import React from 'react'
import { StyledImg, StyledText, Container } from './carousel.styled'
import { Carousel } from '3d-react-carousal'


let slides = [
    <Container>
        <StyledImg src="https://www.e-architect.com/wp-content/uploads/2019/03/black-star-car-wash-in-moscow-g020319-m15.jpg" alt="1" />
        <StyledText color="orange">Быстрое и качественное обслуживание</StyledText>
    </Container>,
    <Container>
        <StyledImg src="https://avatars.mds.yandex.net/get-altay/2887807/2a0000017463279a75018b1eb7ee00650171/XXL" alt="2" />
        <StyledText color="MidnightBlue" top={`40%`}> &nbsp;Доступные цены</StyledText>
    </Container>,
    <Container>
        <StyledImg src="https://cdn.motor1.com/images/mgl/3Z3yR/s1/black-star-car-wash.jpg" alt="3" />
        <StyledText color='Maroon' top={`30%`}>Вежливый персонал</StyledText>
    </Container>,
    <Container>
        <StyledImg src="https://www.treehugger.com/thmb/Olx8_AJw79Br78BTtsuSYW_7BKw=/4500x3000/filters:fill(auto,1)/__opt__aboutcom__coeus__resources__content_migration__mnn__images__2019__07__commercial_car_wash-1d8498e0ea964ffc91eeab5b48c1265b.jpg" alt="4" />
        <StyledText color='Violet'>Богатый ассортимент автокосметики</StyledText>
    </Container>,
    <Container>
        <StyledImg src="https://www.newgateschool.org/assets/uploads/Car_being_Washed.jpg" alt="5" />
        <StyledText color='Teal' top={`30%`}>Удобное время работы </StyledText>
    </Container>,
];

export const Caro = () => {
    return (
        <div style={{height: 440}}>
        <Carousel slides={slides} autoplay={true} />
        </div>


    )
}
