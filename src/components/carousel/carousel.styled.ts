import styled from 'styled-components';

export const StyledImg = styled.img`
        height: 400px;
        width: 600px;
`
export const StyledText = styled.div`
        font-size: 70px;
        text-align: center;
        font-weight: bold;
        color: ${(props) => props.color ? props.color : 'black'};
        position: absolute;
        top: ${(props) => props.top ? props.top : `20%`};
   

`
export const Container = styled.div`
  position: relative;
  text-align: center;
`