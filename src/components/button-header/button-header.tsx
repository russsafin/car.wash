import React from 'react';

import { StyledButton } from './button-header.styled';

const Button = ({ text }) => (<StyledButton> {text} </StyledButton>);

export default Button;