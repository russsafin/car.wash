import styled from 'styled-components';

export const StyledButton = styled.button`
background: transparent;
font: 24px Helvetica;
color: black;
margin: 10px 10px;
padding: 15px 15px;
border: 0;

&:hover {
    background: #0EB6CC;
    color: white;
  }
`
