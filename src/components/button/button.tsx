import React from 'react';
import Button from '@material-ui/core/Button'

import { StyledButton } from './button.styled';
const ButtonClick = (props) =>  (
    <StyledButton>
        <Button 
        variant="contained" 
        color='primary'
        size='large'
        >
           {props.title}
        </Button>
    </StyledButton>)
export default ButtonClick;