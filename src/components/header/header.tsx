import React from 'react';
import GroupButton from '../group-button/group-button';
import LogoText from '../logo-text/logo-text';
import { StyledHeader } from './header.styled';

export const Header = () => (<StyledHeader>
    <thead>
        <tr>
            <th>
                <LogoText/>
            </th>
            <GroupButton/>
        </tr>
     </thead>
</StyledHeader>);

