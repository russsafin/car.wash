import styled from 'styled-components';

export const StyledLogoText = styled.h1`
    font: 33px Helvetica;
    margin-left: 10px;
    font-weight: bold;
`
