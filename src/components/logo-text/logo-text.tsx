import React from 'react';
import { StyledLogoText } from './logo-text.styled';


const LogoText = () => (
    <th>
        <StyledLogoText>
            Car Wash
        </StyledLogoText>
    </th>
)
export default LogoText;

