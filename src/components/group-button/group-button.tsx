import React from 'react';
import {useState} from 'react'
import Button from '../button-header';
import { StyledGroupButton } from './group-button.styled';


const GroupButton = () => {

    return (
        <StyledGroupButton>
            <th>
                <Button text={'Главная страница'} />
            </th>
            <th>

                <Button text={'Карта'} />
            </th>
            <th>
                <Button text={'Регистрация'} />
            </th>
            <th>
                <Button text={'Цены'} />
            </th>
        </StyledGroupButton>
    )

};
export default GroupButton;