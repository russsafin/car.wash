import React from 'react'
import { StyledAchievLine, StyledImg, StyledDescription,Div } from './achievline.styled'
import eight from '../../../media/achievline/8.png'
import onehundredfifthythousand from '../../../media/achievline/150000.png'
import thousand from '../../../media/achievline/1000.png'
import threehundred from '../../../media/achievline/300.png'
import twelve from '../../../media/achievline/12.png'
import ninethousand from '../../../media/achievline/9000.png'

export const AchievLine = () => {
    return (
        <StyledAchievLine>
                <Div>
                    <StyledImg src={eight} alt ='8'/>
                    <StyledDescription color="red">лет на рынке автомоек</StyledDescription>
                </Div>
                <Div>
                    <StyledImg src={onehundredfifthythousand}/>
                    <StyledDescription>клиентов воспользовались нашими услугами</StyledDescription>
                </Div>
                <Div>
                    <StyledImg src={thousand} />
                    <StyledDescription>опытных профессиона- лов, знающих своё дело</StyledDescription>
                </Div>
                <Div>
                    <StyledImg src={threehundred} />
                    <StyledDescription>автомоек по всей стране</StyledDescription>
                </Div>
                <Div>
                    <StyledImg src={twelve} />
                    <StyledDescription>процентов доли на рынке</StyledDescription>
                </Div>
                <Div>
                    <StyledImg src={ninethousand} />
                    <StyledDescription>человек порекомендовали нас другим</StyledDescription>
                </Div>

        </StyledAchievLine>
    )
}