import React, { useEffect, useState } from 'react';
import { StyledMain, Background } from './main.styled';
import Caro from '../../components/carousel'
import Header from '../../components/header'
import ButtonClick from '../../components/button'
import AchievLine from '../../components/achiev-line'

export const Main = () => {
    return (
        <Background>
            <StyledMain>
                <Header />
                <div id="title">
                    Добро пожаловать в<a id="name"> Car.Wash</a>
                </div>
                <Caro />
                <ButtonClick title="Помыть машину" />
                <AchievLine />
            </StyledMain>
        </Background>

    )
}