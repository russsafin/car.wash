import styled from 'styled-components';

export const StyledMain = styled.div`
    #title {
        font-size: 58px;
        font-weight: bold;
        text-align: center;
        margin: 2%;
    }
    #name {
        font-family: Gill Sans Extrabold, sans-serif;
        color: gold;
    };
`
export const Background = styled.div`
    background-image: url('./backgroundimage.jpg');
`
