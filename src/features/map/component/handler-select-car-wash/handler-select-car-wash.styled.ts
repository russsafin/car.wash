import styled from 'styled-components';

export const StyledHandlerSelectCarWash = styled.div`
    text-align: left;
    option, select {
        display: inline-block;
        width: 250px;
        height: 34px;
        line-height: 30px;
        position: relative;
        font: 18px Helvetica;
    }
`
