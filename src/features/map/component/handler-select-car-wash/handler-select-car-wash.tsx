import React from 'react';
import { StyledHandlerSelectCarWash } from './handler-select-car-wash.styled';
import ButtonContent from '../button-content/button-content';

const HandlerSelectCarWash = () => {
    return(
        <div>
            <p>Автомойка “Моем машину”</p>
            <p>Адрес: Труженников 2-пер, д. 7</p>
            <p>Режим работы: 8:00 - 23:00</p>
            <p>Телефон: 8-123-456-78-91</p>
            <p>Доступное время на сегодня:</p>
            <StyledHandlerSelectCarWash>
                <select>
                    <option value="threeInOne">Мойка 3 в 1</option>
                    <option value="cabinWashing">Мойка салона</option>
                    <option value="nonContactWashing">Бесконтактная мойка</option>
                    <option value="nanoWash">Нано мойка</option>
                </select>
            </StyledHandlerSelectCarWash>
            <p>Доступное время на сегодня:</p>
            <StyledHandlerSelectCarWash>
                <select>
                    <option value="threeInOne">13:00 - 14:00</option>
                    <option value="cabinWashing">16:00 - 17:00</option>
                    <option value="nonContactWashing">18:00 - 19:00</option>
                    <option value="nanoWash">20:00 - 21:00</option>
                </select>
          </StyledHandlerSelectCarWash>
          <ButtonContent text={'Записаться'}/>
        </div>
    )
}

export default HandlerSelectCarWash;