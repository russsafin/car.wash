import React from 'react';

import { StyledContentButton } from './button-content.styled';

const ButtonContent = ({ text }) => (<StyledContentButton> {text} </StyledContentButton>);

export default ButtonContent;