import styled from 'styled-components';

export const StyledContentButton = styled.button`
background: transparent;
font: 14px Helvetica;
margin: 3vh 0vh 0vh -4vh;
padding: 2vh 2vh;
border: 0;
background: #0EB6CC;
color: white;
text-align: left;
&:active {
  background: #2E8C99;
}
`
