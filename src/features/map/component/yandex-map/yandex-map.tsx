import React from 'react';
import { StyledYandexMap } from './yandex-map.styled';

function yandexMaps() {
    // @ts-ignore
    ymaps.ready(init);
    function init() {
        // @ts-ignore
        const location = ymaps.geolocation;
        // @ts-ignore
        const myMap = new ymaps.Map('map', {
            center: [55.76, 37.64],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });
    location.get({
        mapStateAutoApply: true
    }).then(
    function(result) {
        const userAddress = result.geoObjects.get(0).properties.get('text');
        const userCoodinates = result.geoObjects.get(0).geometry.getCoordinates();
        result.geoObjects.get(0).properties.set({
            balloonContentBody: 'Адрес: ' + userAddress +
                            '<br/>Координаты:' + userCoodinates
        });
    myMap.geoObjects.add(result.geoObjects)
    },
    function(err) {
        console.log('Ошибка: ' + err)
    }
    );
    }

}

const YandexMap = () => {
    yandexMaps(); 
    return (<>
            <StyledYandexMap id="map"></StyledYandexMap>
    </>);
}
export default YandexMap;