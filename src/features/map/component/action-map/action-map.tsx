import React from 'react';
import { StyledActionMap } from './action-map.styled';
import HandlerSelectCarWash from '../handler-select-car-wash/handler-select-car-wash';

const MessageSelectCarWash = () => {
    return(
        <h2>Выберите автомойку</h2>
    )
}

function HandlerAction(props) {
    if (props.isSelectedMark) {
        return <HandlerSelectCarWash />
    } else {
        return <MessageSelectCarWash />
    }
}

const ActionMap = () => {
    return (
    <>
        <StyledActionMap>
            <HandlerAction isSelectedMark={true}/>
        </StyledActionMap>
    </>
    );
}
export default ActionMap;