import React from 'react';
import YandexMap from '../yandex-map/yandex-map';
import { StyledContentMap } from './content-map.styled';
import ActionMap from '../action-map/action-map';

const ContentMap = () => {
    return (
    <StyledContentMap>
            <th>
                <YandexMap/>
            </th>
            <th>
                <ActionMap/>
            </th>
    </StyledContentMap>);
}

export default ContentMap;