import styled from 'styled-components';

export const StyledMap = styled.map`
    p {
        font: 18px Helvetica;
        font-weight: bold;
        text-align: left;
    }
    h2 {
        font: 33px Helvetica;
        font-weight: bold;
        margin-left: 2em;
        margin-top: 7em;
    }

`
