import React, {useEffect, useState} from 'react';
import ContentMap from './component/content-map/content-map'
import Header from '../../components/header';
import { getConfigValue } from '@ijl/cli'
import { StyledMap } from './map.styled';

export const Map = () => {
    const [ready, setReady] = useState(false)
    useEffect(() => {
        const element = document.createElement('script')
        element.src = `https://api-maps.yandex.ru/2.1/?apikey=${getConfigValue('yandex.map.key')}&lang=ru_RU`
        element.type = 'text/javascript'
        element.async = true
        element.onload = () => {
            setReady(true)
        }
        document.head.appendChild(element)
        return () => { 
            document.head.removeChild(element)
        }

    }, [])

    return (
        <StyledMap>
        <Header/>
        {ready && <ContentMap/>}
        </StyledMap>
    )
}