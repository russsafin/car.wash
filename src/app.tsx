import React from 'react';
import { getAllFeatures } from '@ijl/cli'
import Registration from './features/registration'
import Map from './features/map'
import Main from './features/main'
import Header from './components/header'

const App = () => {
    const features = getAllFeatures()['car.wash'];

    console.log(features);
    return(
        <>
            { features.registration && <Registration /> }
            { features.map && <Map /> }
            { features.main && <Main/>}
        </>
    )
}

export default App;

